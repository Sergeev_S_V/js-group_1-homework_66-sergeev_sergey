import React from 'react';
import './Spinner.css';

const Spinner = props => (
  <div className='Spinner' style={{display: props.loading ? 'block' : 'none'}}>
    Loading...
  </div>
);

export default Spinner;