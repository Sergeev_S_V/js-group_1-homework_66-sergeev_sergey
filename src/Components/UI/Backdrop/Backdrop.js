import React, {Fragment} from 'react';
import './Backdrop.css';
import Spinner from "../Spinner/Spinner";

const Backdrop = props => (
    <Fragment>
      {props.show ? <div className="Backdrop" onClick={props.closed}/> : null}
      {!props.show && props.loading ? <Spinner loading/> : null}
    </Fragment>
);

export default Backdrop;