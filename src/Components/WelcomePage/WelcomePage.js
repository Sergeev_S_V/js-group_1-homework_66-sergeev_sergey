import React from 'react';
import './WelcomePage.css';

const WelcomePage = () => (
  <div className='WelcomePage'>
    <p>Welcome to our site</p>
    <div>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium aperiam architecto blanditiis commodi dolorum, id, maiores nisi, officiis qui recusandae repellat reprehenderit repudiandae sapiente ut!</div>
    <div>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium aperiam architecto blanditiis commodi dolorum, id, maiores nisi, officiis qui recusandae repellat reprehenderit repudiandae sapiente ut!</div>
    <div>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium aperiam architecto blanditiis commodi dolorum, id, maiores nisi, officiis qui recusandae repellat reprehenderit repudiandae sapiente ut!</div>
  </div>
);

export default WelcomePage;