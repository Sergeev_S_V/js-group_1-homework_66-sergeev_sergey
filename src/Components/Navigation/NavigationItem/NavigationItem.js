import React from 'react';
import {NavLink} from "react-router-dom";

const NavigationItem = props => (
    <li><NavLink to={props.to}>{props.children}</NavLink></li>
);

export default NavigationItem;