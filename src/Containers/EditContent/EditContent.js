import React, {Component} from 'react';
import axios from 'axios';
import ReactQuill from 'react-quill';
import 'react-quill/dist/quill.snow.css';

import './EditContent.css';
import withLoadingHandler from "../../hoc/withLoadingHandler/withLoadingHandler";

const navMenu = [
  {title: 'About', id: 'about'},
  {title: 'Cooperation', id: 'cooperation'},
  {title: 'Press-center', id: 'press-center'},
  {title: 'Career', id: 'career'},
  {title: 'Contact-info', id: 'contact-info'},
];

class EditContent extends Component {

  state = {
    title: '',
    content: '',
    selectName: navMenu[0].id,
  };

  getContent = () => {
    axios.get(`/${this.state.selectName}.json`)
      .then(response =>
        this.setState({title: response.data.title, content: response.data.content}));
  };

  changeSelectHandler = event => {
    this.setState({selectName: event.target.value},
      () => this.getContent());
  };

  changeTitleHandler = event => {
    this.setState({title: event.target.value});
  };

  changeContentHandler = value => {
    this.setState({content: value});
  };

  saveEditing = () => {
    axios.patch(`/${this.state.selectName}.json`,
      {title: this.state.title, content: this.state.content})
      .then(() => this.props.history.push(`/pages/${this.state.selectName}`));
  };

  componentDidMount() {
    this.getContent();
  };

  render() {
    return(
      <div className='EditContent'>
        <p className='Edit'>Edit pages</p>
        <p className='Edit-Select'>Select page</p>
        <select className='Select btn btn-default'
                onChange={this.changeSelectHandler}
                value={this.state.selectName} name="" id="">
          {navMenu.map(conf => <option value={conf.id} key={conf.id}>{conf.title}</option>)}
        </select>
        <p className='Edit-Title'>Title</p>
        <input className='Input form-control' onChange={this.changeTitleHandler}
               value={this.state.title} type="text"/>
        <p className='Edit-Content'>Content</p>
        <div className='Textarea'>
          <ReactQuill value={this.state.content}
                      onChange={this.changeContentHandler}/>
        </div>
        <button className='btn btn-default'
                onClick={this.saveEditing}>Save</button>
      </div>
    );
  }
}

export default withLoadingHandler(EditContent, axios);