import React, {Component} from 'react';
import axios from 'axios';
import './Content.css';
import withLoadingHandler from "../../hoc/withLoadingHandler/withLoadingHandler";

class Content extends Component {

  state = {
    currentContent: {},
  };

  getContent = () => {
    axios.get(`/${this.props.match.params.content}.json`)
      .then(response => response.data
        ? this.setState({currentContent: response.data})
        : null
      );
  };

  componentDidUpdate(prevProps) {
    if (this.props.match.params.content !== prevProps.match.params.content) {
      this.getContent();
    }
  };

  componentDidMount() {
    this.getContent();
  };

  render() {
    return (
      <div className='Content-Block'>
        <h3 className='Title'>{this.state.currentContent.title}</h3>
        <div className='Content' dangerouslySetInnerHTML={{__html: this.state.currentContent.content}}/>
        <div className='Content' dangerouslySetInnerHTML={{__html: this.state.currentContent.content}}/>
      </div>
    )
  }
}

export default withLoadingHandler(Content, axios);