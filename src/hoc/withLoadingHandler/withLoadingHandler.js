import React, {Component, Fragment} from 'react';
import Modal from "../../Components/UI/Modal/Modal";

const withLoadingHandler = (WrappedComponent, axios) => {
  return class extends Component {
    constructor(props) {
      super(props);

      this.state = {
        error: null,
        loading: false,
      };

      this.requestInterceptorId = axios.interceptors.request.use((req) => {
        this.setState({loading: true});
        return req;
      });
      this.responseInterceptorId = axios.interceptors.response.use(res => {
        this.setState({loading: false});
        return res;
      }, (error) => {
        this.setState({error, loading: false});
        return error;
      });
    }

    componentWillUnmount() {
      axios.interceptors.response.eject(this.responseInterceptorId);
      axios.interceptors.request.eject(this.requestInterceptorId);
    };

    errorDismissed = () => {
      this.setState({error: null});
    };

    render() {
      return (
        <Fragment>
          <Modal show={this.state.error}
                 loading={this.state.loading}
                 closed={this.errorDismissed}>
            Something wrong happened</Modal>
          <WrappedComponent {...this.props}/>
        </Fragment>
      )
    }
  }
};

export default withLoadingHandler;